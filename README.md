# README #

-- How do I start contributing? --

Well, you can only contribute to branch called "community". If your edits are valid they will be sent to "dev" branch where they will be further edited and then will go to "master"
You need to have some sort of "git" client, my favorite is SourceTree or the GitHub's one.
I would also recommend using NetBeans IDE full pack because it's really useful and free development bundle.

-- What do I need to know about the project? --

The project is intended to be made of two parts one is the API, the other one is the interface for users. The API hasn't been started yet (as of 8/31/2015 at least). This is bare bones (as of 8/31/2015) and some things don't work and are done terribly. I haven't included the database file but I will later on... This project is meant to be a social project where people would be able to login and socialize (no, not Facebook or Twitter) something different, I don't know what but not those two, I want it to be unique...
There's no goal, and no specific requirements, any designs will be accepted for now

-- Further Information --

I'm looking for few developers who know PHP, MySQLi, HTML(5), JQuery, JavaScript and CSS for now. If you thing you got what it takes to get this project running email me at: mrfakepie(at)gmail.com with your name and short introduction, also don't forget to mention that you want to help with "social project".
The name myWired is just temporary I just took (my)space and (Wired).com.

-- Even more info --

So if you read this far you might be actually interested in helping. So I aim to host the first live version in about a year (so ~2017) if this works out, I don't aim to make a lot of profit from it, but if it works I might as well (don't worry potential devs, you'll get your share)